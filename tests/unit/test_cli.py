from click.testing import CliRunner

from rcmip import cli


def test_cli_empty():
    runner = CliRunner()
    result = runner.invoke(cli.cli)
    assert result.exit_code == 0
    assert "rcmip" in result.output
    assert "--help  Show this message and exit." in result.output
    assert "Commands:" in result.output


def test_cli_help():
    runner = CliRunner()
    help_result = runner.invoke(cli.cli, ["--help"])
    assert help_result.exit_code == 0
    assert "--help  Show this message and exit." in help_result.output
    assert "Commands:" in help_result.output


def test_cli_upload():
    # Placeholder test until fleshed out
    runner = CliRunner()
    result = runner.invoke(cli.cli, ["upload"])
    assert result.exit_code == 0
    assert "upload" in result.output


def test_cli_validate():
    # Placeholder test until fleshed out
    runner = CliRunner()
    result = runner.invoke(cli.cli, ["validate"])
    assert result.exit_code == 0
    assert "validate" in result.output