rcmip
=======

.. sec-begin-links

+-------------------+-----------+
| Repository health | |License| |
+-------------------+-----------+

+-----------------+------------------+
| Latest releases | |Latest Version| |
+-----------------+------------------+

+-----------------+----------------+---------------+
| Latest activity | |Contributors| | |Last Commit| |
+-----------------+----------------+---------------+

.. |Build Status| image:: https://travis-ci.org/lewisjared/scmdata.svg?branch=master
    :target: https://travis-ci.org/lewisjared/scmdata
.. |Codecov| image:: https://img.shields.io/codecov/c/github/lewisjared/scmdata.svg
    :target: https://codecov.io/gh/lewisjared/scmdata/branch/master/graph/badge.svg
.. |License| image:: https://img.shields.io/github/license/lewisjared/scmdata.svg
    :target: https://gitlab.com/rcmip/rcmip/blob/master/LICENSE
.. |Latest Version| image:: https://img.shields.io/gitlab/tag/rcmip/rcmip.svg
    :target: https://gitlab.com/rcmip/rcmip/releases
.. |Last Commit| image:: https://img.shields.io/gitlab/last-commit/rcmip/rcmip.svg
    :target: https://gitlab.com/rcmip/rcmip/commits/master
.. |Contributors| image:: https://img.shields.io/github/contributors/rcmip/rcmip.svg
    :target: https://gitlab.com/rcmip/rcmip/graphs/contributors

.. sec-begin-index

RCMIP is a work in progress.
We have just submitted the Phase 1 manuscript to GMD and are in the process of tidying up the repository.
If you have any urgent issues, please raise them in the [issue tracker](https://gitlab.com/rcmip/rcmip/issues).

.. sec-end-index

Contributing
------------

If you'd like to contribute, please make a pull request!
The pull request templates should ensure that you provide all the necessary information.

.. sec-begin-license

License
-------

RCMIP is free software under a BSD 3-Clause License, see `LICENSE <https://gitlab.com/rcmip/rcmip/license/blob/master/LICENSE>`_.

.. sec-end-license