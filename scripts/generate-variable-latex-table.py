import click
import pandas as pd


def use_underscore(inu, use_chem=False):
    underscore_replacements = {
        "CO2": "CO_2",
        "CH4": "CH_4",
        "N2O": "N_2O",
        "NF3": "NF_3",
        "CF4": "CF_4",
        "C2F6": "C_2F_6",
        "C3F8": "C_3F_8",
        "C4F10": "C_4F_10",
        "C5F12": "C_5F_12",
        "C6F14": "C_6F_14",
        "C7F16": "C_7F_16",
        "C8F18": "C_8F_18",
        "c-C4F8": "c-C_4F_8",
        "SF6": "SF_6",
        "SO2F2": "SO_2F_2",
        "SO2": "SO_2",
        "CCl4": "CCl_4",
        "CH2Cl2": "CH_2Cl_2",
        "CH3Br": "CH_3Br",
        "CH3CCl3": "CH_3CCl_3",
        "CH3Cl": "CH_3Cl",
        "CHCl3": "CHCl_3",
        "/yr": " yr^{-1}",
        "/m^2": " m^{-2}",
        "/K": " K^{-1}",
    }
    outu = inu
    for old, new in underscore_replacements.items():
        if use_chem:
            outu = outu.replace(old, "\\chem{" + new + "}")
        else:
            outu = outu.replace(old, new)

    return outu


def escape_special(ins):
    special_replacements = {
        "ECS_eff": "ECS\\_eff",
        "\\Delta": "Delta",
        "\\lambda": "lambda",
        "lambda_eff": "lambda\\_eff",
        "10^21J": "\\unit{10^{21} J}",
        "'comments'": "`comments'",
    }
    outs = ins
    for old, new in special_replacements.items():
        outs = outs.replace(old, new)

    return outs


@click.command(context_settings={"help_option_names": ["-h", "--help"]})
@click.option(
    "--src",
    type=click.Path(exists=True, dir_okay=False, readable=True, resolve_path=True),
    help="RCMIP protocol to convert",
    required=True,
)
@click.option(
    "--sheet-name",
    type=click.STRING,
    help="RCMIP protocol to convert",
    default="variable_definitions",
)
@click.option(
    "--dst",
    type=click.Path(dir_okay=False, writable=True, resolve_path=True),
    help="File to write output into",
    required=True,
)
def convert_table(src, sheet_name, dst):
    """Convert plain text into a table"""
    df = pd.read_excel(src, sheet_name=sheet_name)

    table_specs = "\\begin{tabularx}{\\textwidth}{p{2cm}p{5cm}p{2cm}p{6cm}p{1cm}}"
    topline = "\\tophline"
    header = "\\textbf{Category} & \\textbf{Variable} & \\textbf{Unit} & \\textbf{Definition} & \\textbf{Tier} \\\\"

    output = [
        "\\begin{table*}[t]",
        "\\caption{",
        "RCMIP Phase 1 variable overview (also available at \\url{rcmip.org}).",
        "}",
        "\\label{tab:rcmip-variable-overview}",
        table_specs,
        topline,
        header,
    ]

    new_table = [
        "\\bottomhline",
        "\\end{tabularx}",
        "\\end{table*}",
        "",
        "\\clearpage",
        "",
        "\\addtocounter{table}{-1}",
        "\\begin{table*}[t]",
        "\\caption{Continued.}",
        table_specs,
        topline,
        header,
    ]

    definition_lines = 0
    variable_lines = 0
    for i, (_, row) in enumerate(df.iterrows()):
        output.append("\\middlehline")
        line = " & ".join([
            row["Category"],
            row["Variable"],
            "\\unit{" + use_underscore(row["Unit"]) + "}",
            escape_special(use_underscore(row["Definition"], use_chem=True)),
            str(row["Tier"])
        ]) + " \\\\"
        output.append(line)
        definition_lines += len(use_underscore(row["Definition"], use_chem=True).split())
        variable_lines += len(row["Variable"])

        # if i > 90:
        #     break
        if (definition_lines > 150) or (variable_lines > 450):
            for r in new_table:
                output.append(r)
            definition_lines = 0
            variable_lines = 0

    output.append("\\bottomhline")
    output.append("\\end{tabularx}")
    output.append("\\end{table*}")
    with open(dst, "w") as f:
        f.write("\n".join(output))


if __name__ == "__main__":
    convert_table()
