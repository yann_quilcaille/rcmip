---- HEADER ----

Date: 2019-12-19 12:35:36
Contact: Zebedee Nicholls <zebedee.nicholls@climate-energy-college.org>, Jared Lewis <jared.lewis@climate-energy-college.org>, Malte Meinshausen <malte.meinshausen@unimelb.edu.au>
Source data crunched with: NetCDF-SCM v2.0.0-beta.6 (more info at gitlab.com/znicholls/netcdf-scm)
File written with: pymagicc v2.0.0-beta.8 (more info at github.com/openclimatedata/pymagicc)


---- METADATA ----

CDI: Climate Data Interface version 1.5.4 (http://code.zmaw.de/projects/cdi)
CDO: Climate Data Operators version 1.5.4 (http://code.zmaw.de/projects/cdo)
Conventions: CF-1.5
area_world (m**2): 509499402446956.3
area_world_el_nino_n3.4 (m**2): 6167472062465.617
area_world_land (m**2): 146401455477158.16
area_world_north_atlantic_ocean (m**2): 39450812175826.164
area_world_northern_hemisphere (m**2): 254749701223478.12
area_world_northern_hemisphere_land (m**2): 98593720745412.48
area_world_northern_hemisphere_ocean (m**2): 156155980477608.56
area_world_ocean (m**2): 363097946996914.44
area_world_southern_hemisphere (m**2): 254749701223478.12
area_world_southern_hemisphere_land (m**2): 47807734731745.65
area_world_southern_hemisphere_ocean (m**2): 206941966519305.8
associated_files: baseURL: http://cmip-pcmdi.llnl.gov/CMIP5/dataLocation gridspecFile: gridspec_atmos_fx_bcc-csm1-1_abrupt4xCO2_r0i0p0.nc areacella: areacella_fx_bcc-csm1-1_abrupt4xCO2_r0i0p0.nc
branch_time: 160.0
calendar: 365_day
cmor_version: 2.5.6
comment: Impose an instantaneous quadrupling of CO2, then hold fixed. Other forcings are fixed at the values of pre-industrial control run. The model initial state is from the pre-industrial control simulation at 1st Dec. of year 160.
contact: Dr. Tongwen Wu (twwu@cma.gov.cn)
creation_date: 2011-07-20T02:38:58Z
crunch_contact: Zebedee Nicholls <zebedee.nicholls@climate-energy-college.org>, Jared Lewis <jared.lewis@climate-energy-college.org>, Malte Meinshausen <malte.meinshausen@unimelb.edu.au>
crunch_netcdf_scm_version: 2.0.0-beta.6 (more info at gitlab.com/znicholls/netcdf-scm)
crunch_source_files: Files: ['/cmip5/abrupt4xCO2/Amon/tas/bcc-csm1-1/r12i1p1/tas_Amon_bcc-csm1-1_abrupt4xCO2_r12i1p1_016012-016512.nc']
experiment: abrupt 4XCO2
experiment_id: abrupt4xCO2
forcing: GHG
frequency: mon
history: 2011-07-20T02:38:57Z altered by CMOR: Treated scalar dimension: 'height'.
initialization_method: 1
institute_id: BCC
institution: Beijing Climate Center(BCC),China Meteorological Administration,China
land_fraction: 0.287343723611923
land_fraction_northern_hemisphere: 0.3870219288654692
land_fraction_southern_hemisphere: 0.18766551835837683
model_id: bcc-csm1-1
modeling_realm: atmos
original_name: TREFHT
parent_experiment: pre-industrial control
parent_experiment_id: piControl
parent_experiment_rip: r1i1p1
physics_version: 1
product: output
project_id: CMIP5
realization: 12
table_id: Table Amon (11 April 2011) 1cfdc7322cf2f4a32614826fab42c1ab
title: bcc-csm1-1 model output prepared for CMIP5 abrupt 4XCO2
tracking_id: b8059894-abd3-481a-9a1d-35cae57d2369

&THISFILE_SPECIFICATIONS
    THISFILE_DATACOLUMNS = 11
    THISFILE_DATAROWS = 6
    THISFILE_FIRSTYEAR = 160
    THISFILE_LASTYEAR = 165
    THISFILE_ANNUALSTEPS = 1
    THISFILE_DATTYPE = 'MAG'
    THISFILE_REGIONMODE = 'NONE'
    THISFILE_TIMESERIESTYPE = 'AVERAGE_YEAR_MID_YEAR'
    THISFILE_FIRSTDATAROW = 76
/

   VARIABLE                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas
       TODO                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET
      UNITS                   K                   K                   K                   K                   K                   K                   K                   K                   K                   K                   K
      YEARS               WORLD                 N34                LAND                 AMV                  NH              NHLAND             NHOCEAN               OCEAN                  SH              SHLAND             SHOCEAN
        160         2.86279e+02         2.98597e+02         2.77740e+02         2.91007e+02         2.83223e+02         2.73743e+02         2.89208e+02         2.89722e+02         2.89335e+02         2.85984e+02         2.90109e+02
        161         2.88289e+02         2.99944e+02         2.82688e+02         2.92836e+02         2.88663e+02         2.84013e+02         2.91598e+02         2.90547e+02         2.87915e+02         2.79954e+02         2.89754e+02
        162         2.88822e+02         2.99783e+02         2.83359e+02         2.93481e+02         2.89318e+02         2.84714e+02         2.92224e+02         2.91025e+02         2.88327e+02         2.80565e+02         2.90120e+02
        163         2.89247e+02         3.00070e+02         2.84093e+02         2.93717e+02         2.89938e+02         2.85619e+02         2.92665e+02         2.91325e+02         2.88556e+02         2.80945e+02         2.90314e+02
        164         2.89401e+02         3.00724e+02         2.84139e+02         2.94154e+02         2.90170e+02         2.85759e+02         2.92955e+02         2.91523e+02         2.88632e+02         2.80797e+02         2.90442e+02
        165         2.89562e+02         2.99882e+02         2.84423e+02         2.94353e+02         2.90376e+02         2.86087e+02         2.93083e+02         2.91634e+02         2.88749e+02         2.80992e+02         2.90541e+02
