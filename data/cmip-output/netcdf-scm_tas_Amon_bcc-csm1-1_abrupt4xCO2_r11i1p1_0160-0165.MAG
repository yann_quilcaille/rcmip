---- HEADER ----

Date: 2019-12-19 12:35:35
Contact: Zebedee Nicholls <zebedee.nicholls@climate-energy-college.org>, Jared Lewis <jared.lewis@climate-energy-college.org>, Malte Meinshausen <malte.meinshausen@unimelb.edu.au>
Source data crunched with: NetCDF-SCM v2.0.0-beta.6 (more info at gitlab.com/znicholls/netcdf-scm)
File written with: pymagicc v2.0.0-beta.8 (more info at github.com/openclimatedata/pymagicc)


---- METADATA ----

CDI: Climate Data Interface version 1.5.4 (http://code.zmaw.de/projects/cdi)
CDO: Climate Data Operators version 1.5.4 (http://code.zmaw.de/projects/cdo)
Conventions: CF-1.5
area_world (m**2): 509499402446956.3
area_world_el_nino_n3.4 (m**2): 6167472062465.617
area_world_land (m**2): 146401455477158.16
area_world_north_atlantic_ocean (m**2): 39450812175826.164
area_world_northern_hemisphere (m**2): 254749701223478.12
area_world_northern_hemisphere_land (m**2): 98593720745412.48
area_world_northern_hemisphere_ocean (m**2): 156155980477608.56
area_world_ocean (m**2): 363097946996914.44
area_world_southern_hemisphere (m**2): 254749701223478.12
area_world_southern_hemisphere_land (m**2): 47807734731745.65
area_world_southern_hemisphere_ocean (m**2): 206941966519305.8
associated_files: baseURL: http://cmip-pcmdi.llnl.gov/CMIP5/dataLocation gridspecFile: gridspec_atmos_fx_bcc-csm1-1_abrupt4xCO2_r0i0p0.nc areacella: areacella_fx_bcc-csm1-1_abrupt4xCO2_r0i0p0.nc
branch_time: 160.0
calendar: 365_day
cmor_version: 2.5.6
comment: Impose an instantaneous quadrupling of CO2, then hold fixed. Other forcings are fixed at the values of pre-industrial control run. The model initial state is from the pre-industrial control simulation at 1st Nov. of year 160.
contact: Dr. Tongwen Wu (twwu@cma.gov.cn)
creation_date: 2011-07-20T02:26:29Z
crunch_contact: Zebedee Nicholls <zebedee.nicholls@climate-energy-college.org>, Jared Lewis <jared.lewis@climate-energy-college.org>, Malte Meinshausen <malte.meinshausen@unimelb.edu.au>
crunch_netcdf_scm_version: 2.0.0-beta.6 (more info at gitlab.com/znicholls/netcdf-scm)
crunch_source_files: Files: ['/cmip5/abrupt4xCO2/Amon/tas/bcc-csm1-1/r11i1p1/tas_Amon_bcc-csm1-1_abrupt4xCO2_r11i1p1_016011-016512.nc']
experiment: abrupt 4XCO2
experiment_id: abrupt4xCO2
forcing: GHG
frequency: mon
history: 2011-07-20T02:26:29Z altered by CMOR: Treated scalar dimension: 'height'.
initialization_method: 1
institute_id: BCC
institution: Beijing Climate Center(BCC),China Meteorological Administration,China
land_fraction: 0.287343723611923
land_fraction_northern_hemisphere: 0.3870219288654692
land_fraction_southern_hemisphere: 0.18766551835837683
model_id: bcc-csm1-1
modeling_realm: atmos
original_name: TREFHT
parent_experiment: pre-industrial control
parent_experiment_id: piControl
parent_experiment_rip: r1i1p1
physics_version: 1
product: output
project_id: CMIP5
realization: 11
table_id: Table Amon (11 April 2011) 1cfdc7322cf2f4a32614826fab42c1ab
title: bcc-csm1-1 model output prepared for CMIP5 abrupt 4XCO2
tracking_id: 37bb16cc-d386-4f1b-b8a7-30c85385a977

&THISFILE_SPECIFICATIONS
    THISFILE_DATACOLUMNS = 11
    THISFILE_DATAROWS = 6
    THISFILE_FIRSTYEAR = 160
    THISFILE_LASTYEAR = 165
    THISFILE_ANNUALSTEPS = 1
    THISFILE_DATTYPE = 'MAG'
    THISFILE_REGIONMODE = 'NONE'
    THISFILE_TIMESERIESTYPE = 'AVERAGE_YEAR_MID_YEAR'
    THISFILE_FIRSTDATAROW = 76
/

   VARIABLE                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas
       TODO                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET
      UNITS                   K                   K                   K                   K                   K                   K                   K                   K                   K                   K                   K
      YEARS               WORLD                 N34                LAND                 AMV                  NH              NHLAND             NHOCEAN               OCEAN                  SH              SHLAND             SHOCEAN
        160         2.86552e+02         2.98904e+02         2.78379e+02         2.91951e+02         2.84396e+02         2.75473e+02         2.90030e+02         2.89847e+02         2.88707e+02         2.84374e+02         2.89708e+02
        161         2.88471e+02         3.00260e+02         2.82951e+02         2.92888e+02         2.88762e+02         2.84250e+02         2.91610e+02         2.90696e+02         2.88180e+02         2.80272e+02         2.90006e+02
        162         2.88957e+02         2.99889e+02         2.83660e+02         2.93444e+02         2.89495e+02         2.85131e+02         2.92250e+02         2.91092e+02         2.88419e+02         2.80628e+02         2.90219e+02
        163         2.89254e+02         3.01064e+02         2.83973e+02         2.93828e+02         2.89887e+02         2.85486e+02         2.92665e+02         2.91383e+02         2.88621e+02         2.80854e+02         2.90415e+02
        164         2.89532e+02         3.00169e+02         2.84546e+02         2.93992e+02         2.90286e+02         2.86228e+02         2.92848e+02         2.91543e+02         2.88778e+02         2.81078e+02         2.90557e+02
        165         2.89701e+02         3.01259e+02         2.84537e+02         2.94278e+02         2.90428e+02         2.85981e+02         2.93235e+02         2.91783e+02         2.88974e+02         2.81560e+02         2.90687e+02
