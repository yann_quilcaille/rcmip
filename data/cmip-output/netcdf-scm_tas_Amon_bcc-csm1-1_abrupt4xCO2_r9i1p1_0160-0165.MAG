---- HEADER ----

Date: 2019-12-19 12:35:33
Contact: Zebedee Nicholls <zebedee.nicholls@climate-energy-college.org>, Jared Lewis <jared.lewis@climate-energy-college.org>, Malte Meinshausen <malte.meinshausen@unimelb.edu.au>
Source data crunched with: NetCDF-SCM v2.0.0-beta.6 (more info at gitlab.com/znicholls/netcdf-scm)
File written with: pymagicc v2.0.0-beta.8 (more info at github.com/openclimatedata/pymagicc)


---- METADATA ----

CDI: Climate Data Interface version 1.5.4 (http://code.zmaw.de/projects/cdi)
CDO: Climate Data Operators version 1.5.4 (http://code.zmaw.de/projects/cdo)
Conventions: CF-1.5
area_world (m**2): 509499402446956.3
area_world_el_nino_n3.4 (m**2): 6167472062465.617
area_world_land (m**2): 146401455477158.16
area_world_north_atlantic_ocean (m**2): 39450812175826.164
area_world_northern_hemisphere (m**2): 254749701223478.12
area_world_northern_hemisphere_land (m**2): 98593720745412.48
area_world_northern_hemisphere_ocean (m**2): 156155980477608.56
area_world_ocean (m**2): 363097946996914.44
area_world_southern_hemisphere (m**2): 254749701223478.12
area_world_southern_hemisphere_land (m**2): 47807734731745.65
area_world_southern_hemisphere_ocean (m**2): 206941966519305.8
associated_files: baseURL: http://cmip-pcmdi.llnl.gov/CMIP5/dataLocation gridspecFile: gridspec_atmos_fx_bcc-csm1-1_abrupt4xCO2_r0i0p0.nc areacella: areacella_fx_bcc-csm1-1_abrupt4xCO2_r0i0p0.nc
branch_time: 160.0
calendar: 365_day
cmor_version: 2.5.6
comment: Impose an instantaneous quadrupling of CO2, then hold fixed. Other forcings are fixed at the values of pre-industrial control run. The model initial state is from the pre-industrial control simulation at 1st Sep. of year 160.
contact: Dr. Tongwen Wu (twwu@cma.gov.cn)
creation_date: 2011-07-20T02:01:34Z
crunch_contact: Zebedee Nicholls <zebedee.nicholls@climate-energy-college.org>, Jared Lewis <jared.lewis@climate-energy-college.org>, Malte Meinshausen <malte.meinshausen@unimelb.edu.au>
crunch_netcdf_scm_version: 2.0.0-beta.6 (more info at gitlab.com/znicholls/netcdf-scm)
crunch_source_files: Files: ['/cmip5/abrupt4xCO2/Amon/tas/bcc-csm1-1/r9i1p1/tas_Amon_bcc-csm1-1_abrupt4xCO2_r9i1p1_016009-016512.nc']
experiment: abrupt 4XCO2
experiment_id: abrupt4xCO2
forcing: GHG
frequency: mon
history: 2011-07-20T02:01:34Z altered by CMOR: Treated scalar dimension: 'height'.
initialization_method: 1
institute_id: BCC
institution: Beijing Climate Center(BCC),China Meteorological Administration,China
land_fraction: 0.287343723611923
land_fraction_northern_hemisphere: 0.3870219288654692
land_fraction_southern_hemisphere: 0.18766551835837683
model_id: bcc-csm1-1
modeling_realm: atmos
original_name: TREFHT
parent_experiment: pre-industrial control
parent_experiment_id: piControl
parent_experiment_rip: r1i1p1
physics_version: 1
product: output
project_id: CMIP5
realization: 9
table_id: Table Amon (11 April 2011) 1cfdc7322cf2f4a32614826fab42c1ab
title: bcc-csm1-1 model output prepared for CMIP5 abrupt 4XCO2
tracking_id: 63a5e254-6b64-499f-af53-cdcb4b706a53

&THISFILE_SPECIFICATIONS
    THISFILE_DATACOLUMNS = 11
    THISFILE_DATAROWS = 6
    THISFILE_FIRSTYEAR = 160
    THISFILE_LASTYEAR = 165
    THISFILE_ANNUALSTEPS = 1
    THISFILE_DATTYPE = 'MAG'
    THISFILE_REGIONMODE = 'NONE'
    THISFILE_TIMESERIESTYPE = 'AVERAGE_YEAR_MID_YEAR'
    THISFILE_FIRSTDATAROW = 76
/

   VARIABLE                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas
       TODO                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET
      UNITS                   K                   K                   K                   K                   K                   K                   K                   K                   K                   K                   K
      YEARS               WORLD                 N34                LAND                 AMV                  NH              NHLAND             NHOCEAN               OCEAN                  SH              SHLAND             SHOCEAN
        160         2.87323e+02         2.99488e+02         2.80783e+02         2.93049e+02         2.87408e+02         2.80685e+02         2.91652e+02         2.89959e+02         2.87238e+02         2.80987e+02         2.88682e+02
        161         2.88396e+02         2.99866e+02         2.82736e+02         2.92865e+02         2.88706e+02         2.84064e+02         2.91637e+02         2.90678e+02         2.88086e+02         2.79997e+02         2.89954e+02
        162         2.88834e+02         2.99672e+02         2.83392e+02         2.93211e+02         2.89262e+02         2.84782e+02         2.92091e+02         2.91029e+02         2.88406e+02         2.80524e+02         2.90227e+02
        163         2.89303e+02         3.01503e+02         2.84005e+02         2.93440e+02         2.89885e+02         2.85472e+02         2.92672e+02         2.91439e+02         2.88720e+02         2.80979e+02         2.90509e+02
        164         2.89468e+02         2.99444e+02         2.84268e+02         2.93954e+02         2.90158e+02         2.85788e+02         2.92917e+02         2.91564e+02         2.88778e+02         2.81134e+02         2.90543e+02
        165         2.89664e+02         3.01785e+02         2.84514e+02         2.94142e+02         2.90441e+02         2.86068e+02         2.93202e+02         2.91741e+02         2.88888e+02         2.81311e+02         2.90638e+02
