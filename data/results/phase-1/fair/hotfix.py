import glob
import os.path

from scmdata import ScmDataFrame
import tqdm

for f in tqdm.tqdm(glob.glob("./*.csv")):
    tmp = ScmDataFrame(f).timeseries().reset_index()
    tmp["unit"] = tmp["unit"].apply(lambda x: "W/m^2/K" if x == "K/W/m^2" else x)
    tmp = ScmDataFrame(tmp)
    print(os.path.basename(f))
    tmp.to_csv(os.path.basename(f).replace("v1-0-0", "v1-0-1"))

