#!/bin/bash
for f in ./*.csv; do
    echo ${f}
    tmp=${f#*.csv}
    tmpmodel=${f%.csv}
    tmpmodel=${tmpmodel//_/-}
    tmpmodel=${tmpmodel//-rcmip-phase-1/}
    modelcfg=${tmpmodel#./}
    modelcfg=$(echo "${modelcfg}" | tr '[:upper:]' '[:lower:]')
    echo "rcmip_phase-1_${modelcfg}_v1-0-0.csv"
    mv -v ${f} "rcmip_phase-1_${modelcfg}_v1-0-0.csv"
done
