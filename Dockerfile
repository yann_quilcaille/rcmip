FROM continuumio/miniconda3:latest

MAINTAINER Zebedee Nicholls <zebedee.nicholls@climate-energy-college.org>


RUN apt-get update && apt-get install -qq software-properties-common build-essential wget git && conda update --yes conda
RUN useradd -m py
WORKDIR /opt/rcmip

# Create new conda environment and install our requirements too
# and copy data and do any pre-processing required
COPY environment.yml Makefile /opt/rcmip/
RUN conda env create -q -n rcmip && \
    bash -c '. /opt/conda/etc/profile.d/conda.sh && \
            conda activate rcmip && \
            make -B conda-environment \
            && rm -rf /src/* \
            && conda list && \
             pip list' && \
    conda clean -a -y

#RUN echo ". /opt/conda/etc/profile.d/conda.sh;conda activate rcmip" | tee -a ~/.bashrc
RUN sed -i 's/conda activate base/conda activate rcmip/g' ~/.bashrc

# Ensure that the correct conda environment is used
ENV DATA_DIR /opt/rcmip/data
ENV OUTPUT_DIR /opt/rcmip/output

# Copy the src across in two goes to make use of the cached data
COPY data /opt/rcmip/
