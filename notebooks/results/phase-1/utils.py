import numpy as np
import pyam
import scipy.stats
import tqdm
import warnings

from scmdata import ScmDataFrame, df_append


def convert_scmdf_to_pyamdf_year_only(iscmdf):
    out = iscmdf.timeseries()
    out.columns = out.columns.map(lambda x: x.year)

    return pyam.IamDataFrame(out)


def prep_str_for_filename(ins):
    return (
        ins.replace("_", "-")
        .replace("|", "-")
        .replace(" ", "-")
        .replace("(", "")
        .replace(")", "")
        .lower()
    )


def load_database_files(file_list):
    db = []
    for rf in tqdm.tqdm_notebook(file_list):
        if rf.endswith(".csv"):
            loaded = ScmDataFrame(rf)
        else:
            loaded = ScmDataFrame(rf, sheet_name="your_data")

        db.append(loaded)

    db = df_append(db).timeseries().reset_index()
    db["unit"] = db["unit"].apply(
        lambda x: x.replace("Dimensionless", "dimensionless")
        if isinstance(x, str)
        else x
    )
    db = ScmDataFrame(db)

    return db




def aggregate_variable(idf, var_to_agg, models_components):
    db_plus_agg = idf.copy()
    for cm, components in tqdm.tqdm_notebook(
        models_components.items(), desc="Climate models"
    ):
        cm_var = idf.filter(
            climatemodel=cm, variable=[var_to_agg, "{}|*th quantile".format(var_to_agg)]
        )
        if components is None:
            if cm_var.timeseries().empty:
                warnings.warn("No {} data for {}".format(var_to_agg, cm))
            continue

        assert cm_var.timeseries().empty
        cm_components = idf.filter(climatemodel=cm, variable=components)
        if cm_components.timeseries().empty:
            warnings.warn("No {} data for {}".format(components, cm))
            continue

        assert set(components) == set(cm_components["variable"].unique()), "{}: {}".format(cm, set(
                    components
                ) - set(cm_components["variable"].unique()))
        agg = (
            cm_components.timeseries()
            .unstack("variable")
            .stack("time")
            .sum(axis=1)
            .to_frame()
        )
        agg.columns = [var_to_agg]
        agg.columns.names = ["variable"]
        agg = agg.unstack("time").stack("variable")

        db_plus_agg = db_plus_agg.append(agg)

    return db_plus_agg


def aggregate_effective_aerosol_forcing(idf):
    var_to_agg = "Effective Radiative Forcing|Anthropogenic|Aerosols"
    aerosol_forcing_components = {
        "Cicero-SCM*": None,  # already aggregated
        "ESCIMO,rcmip,base": None,  # already aggregated
        "FaIR-1.5*": None,  # already aggregated
        "OSCARv3.0": None,  # already aggregated
        "WASP,2,PROB*": None,  # already aggregated
        "MAGICC7.1.0.beta*": [
            "Effective Radiative Forcing|Anthropogenic|Aerosols|Aerosols-radiation Interactions",
            "Effective Radiative Forcing|Anthropogenic|Aerosols|Aerosols-cloud Interactions",
        ],
        "GIR*": [
            "Effective Radiative Forcing|Anthropogenic|Other|Sulfate A-rI + Aerosol A-cI",
            "Effective Radiative Forcing|Anthropogenic|Aerosols|Aerosols-radiation Interactions|BC and OC|BC",
            "Effective Radiative Forcing|Anthropogenic|Aerosols|Aerosols-radiation Interactions|BC and OC|OC",
            "Effective Radiative Forcing|Anthropogenic|Aerosols|Aerosols-radiation Interactions|Biomass Burning|NH3",
            "Effective Radiative Forcing|Anthropogenic|Aerosols|Aerosols-radiation Interactions|Nitrate",
        ],
    }

    return aggregate_variable(idf, var_to_agg, aerosol_forcing_components)


def reduce_to_one_variant_per_model(indf):
    outdf = []
    for cm in (
        indf["climatemodel"].apply(lambda x: "_".join(x.split("_")[:-1])).unique()
    ):
        variants = sorted(
            indf.filter(climatemodel="{}*".format(cm))["climatemodel"]
            .apply(lambda x: x.split("_")[-1])
            .unique(),
            key=len,
        )
        first_variant = variants[0]

        outdf.append(indf.filter(climatemodel="{}_{}".format(cm, first_variant)))

    outdf = pyam.concat(outdf)

    return outdf


def get_interest_period_mean(idf, interest_period):
    return (
        ScmDataFrame(idf)
        .filter(year=range(interest_period[0], interest_period[1] + 1))
        .timeseries()
        .mean(axis=1)
    )


def get_interest_period_slope(idf, interest_period):
    pdf_interest_period = (
        ScmDataFrame(idf)
        .filter(year=range(interest_period[0], interest_period[1] + 1))
        .timeseries()
    )

    def calc_slope(row):
        # thanks to https://stackoverflow.com/a/47635289
        times = row.index.map(lambda x: x.year)
        assert len(times) == len(set(times))
        np.testing.assert_array_equal(times[1:] - times[:-1], 1)

        a = scipy.stats.linregress(x=times, y=row.values)

        return a.slope

    return pdf_interest_period.apply(calc_slope, axis=1)

def clean_label(inl, ignore=[]):
    if inl.startswith("ACC2"):
        return "ACC2-v4-2"

    if inl.startswith("ar5ir2box"):
        return "ar5ir-2box"

    if inl.startswith("ar5ir3box"):
        return "ar5ir-3box"

    if inl.startswith("Cicero-SCM"):
        return "CICERO-SCM"

    if inl.startswith("ESCIMO"):
        return "ESCIMO"

    if inl.startswith("FaIR-1.5"):
        return "FaIR-v1-5"

    if inl.startswith("GIR (3 box)"):
        return "GIR-3box"

    if inl.startswith("GREB"):
        return "GREB-v1-0-1"

    if inl.startswith("hector"):
        return "hector|62381e71"

    if inl.startswith("held-two-layer"):
        return "held-two-layer-uom"

    if inl.startswith("MAGICC7.1.0.beta"):
        return "MAGICC-v7-1-0-beta"

    if inl.startswith("MCE"):
        return "MCE-v1-1"

    if inl.startswith("OSCAR"):
        return "OSCAR-v3-0"

    if inl.startswith("WASP"):
        return "WASP-v2"

    if inl.startswith("Observations"):
        return "Observations"

    if inl.startswith("CMIP5"):
        return "CMIP5"

    if inl.startswith("CMIP6"):
        return "CMIP6"

    if inl == "CMIP":
        return "CMIP"

    if inl == "RCMIP":
        return "RCMIP"

    if ignore:
        if inl in ignore:
            return inl

    raise NotImplementedError(inl)


def clean_labels(ax, ignore=[]):
    for l in ax.get_lines():
        label = l.get_label()
        if not label.startswith("_"):
            l.set_label(clean_label(label, ignore=ignore))
